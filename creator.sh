#!/bin/bash

#######################################
## t_creator
#######################################

version="v.0.0.6";

init() {
    startCounter=$(date +%s)
    clear

    if [ ! -f "creator.ini" ]; then
        errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <creator.ini> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #include config to create a new project
    . creator.ini

    projectName=$projectname
    repoDocker=$repodocker
    repoInstaller=$repoinstaller
    repoBoilerplate=$repoboilerplate
    repoThm=$repothm
    repoDeploy=$repodeploy

    infoMsg "::\n" \
    ":: Variables:\n" \
    ":: projectName: $projectname\n" \
    ":: repoDocker: $repodocker\n" \
    ":: repoInstaller: $repoinstaller\n" \
    ":: repoBoilerplate: $repoboilerplate\n" \
    ":: repoThm: $repothm\n" \
    ":: repoDeploy: $repodeploy\n" \
    "::\n"

	if [ -d "$projectName" ]; then
        errorMsg ":: Error [bckprh988g5o5e2mw9v1dd894ng17vsp] in configuration\n" \
        ":: Folder <${projectName}> does already exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$projectName" == "projectfoldername" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$projectname must not be '$projectName'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #create projectfolder
    mkdir $projectName

    logMsg "create $projectName folder"

    #go into projectfolder
    cd $projectName

    if [ "$repoDocker" == "XXX_YY_xxx_docker" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repodocker must not be '$repoDocker'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$repoDocker" == "t_docker" ]; then
        errorMsg ":: Error [z1346r85f99w78280p59922xvb33a3ik] in configuration\n" \
        ":: \$repodocker must not be '$repoDocker'\n" \
        ":: Please use only forks for your project\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #make shure .git is removed
    rm -rf .git

    #clone t_docker fork
    logMsg "start cloning ${repoDocker}"
    git init
    git clone https://bitbucket.org/teufels/${repoDocker} t_docker
    sleep 1

    if [ "$repoInstaller" == "XXX_YY_xxx_installer" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repoinstaller must not be '$repoInstaller'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$repoInstaller" == "t_installer_typo3" ]; then
        errorMsg ":: Error [z1346r85f99w78280p59922xvb33a3ik] in configuration\n" \
        ":: \$repoinstaller must not be '$repoInstaller'\n" \
        ":: Please use only forks for your project\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #make shure .git is removed
    rm -rf .git

    #clone t_installer fork
    logMsg "start cloning ${repoInstaller}"
    git init
    git clone https://bitbucket.org/teufels/${repoInstaller} t_installer
    sleep 1

    if [ "$repoBoilerplate" == "XXX_YY_xxx_boilerplate" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repoboilerplate must not be '$repoBoilerplate'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$repoBoilerplate" == "t_php_boilerplate_df" ]; then
        errorMsg ":: Error [z1346r85f99w78280p59922xvb33a3ik] in configuration\n" \
        ":: \$repoboilerplate must not be '$repoBoilerplate'\n" \
        ":: Please use only forks for your project\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #make shure .git is removed
    rm -rf .git

    #clone t_boilerplate fork
    time=$(date +"%Y-%m-%d_%H-%M-%S")
    logMsg "start cloning ${repoBoilerplate}"
    git init
    git clone https://bitbucket.org/teufels/${repoBoilerplate} t_boilerplate_${time}
    sleep 1

    #remove created .git
    rm -rf .git


    ###### copy forked repo variables to t_installer ######
    cd t_installer
    logMsg "set forked repo variables to t_installer/install.ini"
    sed -ie "s/XXX_YY_xxx_docker/$repoDocker/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    sed -ie "s/XXX_YY_xxx_installer/$repoInstaller/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    sed -ie "s/XXX_YY_xxx_boilerplate/$repoBoilerplate/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    sed -ie "s/XXX_YY_xxx_thm/$repoThm/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    sed -ie "s/XXX_YY_xxx_deploy/$repoDeploy/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    cp -rpv install.local.example.ini install.local.ini
    sed -ie "s/t_boilerplate_YY-mm-dd_HH-MM-SS/t_boilerplate_$time/" install.local.ini
    infoMsg "::\n" \
    ":: diff install.local.inie install.local.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.local.inie ./install.local.ini
    infoMsg "-+"
    nbsp

    rm -rf install.local.inie

    #go back
    cd ../..

    #finish message
    logMsg "finished creator"
    endCounter=$(date +%s)
    totalTime=$(echo "$endCounter - $startCounter" | bc)
    echo " total time:" $(convertsecs $totalTime)
}

convertsecs() {
    h=`expr $1 / 3600`
    m=`expr $1  % 3600 / 60`
    s=`expr $1 % 60`
    printf "%02d:%02d:%02d\n" $h $m $s
}

errorMsg() {
    echo -e "" \
    $(tput setaf 1)::$(tput sgr 0) "\n"\
    $(tput setaf 1)$*$(tput sgr 0) "\n"\
    $(tput setaf 1)::$(tput sgr 0)
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "$*"
}

nbsp() {
	echo ""
}

init