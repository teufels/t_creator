NAME
    t_creator

SYNOPSIS
    make project

DESCRIPTION
    The Creator clones the forked repositories.

REQUIREMENTS
    Git

    See also: https://bitbucket.org/account/user/teufels/projects/DOC.

    Using the make command requires configuration of the corresponding 'creator.ini' and existing forked repositories.

    See also: https://bitbucket.org/teufels/t_creator

INSTRUCTIONS
    - Clone the t_creators' fork into the project folder

    #cd to projectsfolder
    $ cd ../projects

    # Create project folder
    $ mkdir c_<customer_abbreviation>_<YY>_<type_of_project_abbreviation>

    # Switch to project folder
    $ cd c_<customer_abbreviation>_<YY>_<type_of_project_abbreviation>

    # Clone forked t_creator
    $ git clone https://bitucket.org/teufels/<customer_abbreviation>_<YY>_<type_of_project_abbreviation>_creator.git

    # Switch to t_creator's folder
    $ cd c_<customer_abbreviation>_<YY>_<type_of_project_abbreviation>_creator

    # Replace placeholder of creator.ini
    $ nano creator.ini # or edit with an editor

    # Optionally create a .gitignore with e.g. the projectfolder

    # Add changed files
    $ git add creator.ini # optional auch die .gitignore

    # Commit changed files
    $ git commit -m 'modified creator.ini'

    # Push changed files
    $ git push [alias] [branch]

    # run t_creator
    $ make project

    See also: https://bitbucket.org/teufels/documentation/wiki/TYPO3%20Installation


NEXTSTEPS
    - Use initial docker without docker-sync

    #cd to t_docker
    $ cd ../t_docker

    # Create initial docker files and move them to t_boilerplate_YY-mm-dd_HH-MM-SS
    $ make docker plain


ERRORCODES:
    TODO: Helpful description of errorcodes and possible solutions.
    [g3978p78jzk312a0el4v1nd4ut6tb3z8]
    [bckprh988g5o5e2mw9v1dd894ng17vsp]
    [dz67w45uqa1562czv25423r46w99k2te]
    [z1346r85f99w78280p59922xvb33a3ik]

LISENCE:
    The MIT License (MIT)

    Copyright (c) 2016
    Andreas Hafner <a.hafner@teufels.com>,
    Dominik Hilser <d.hilser@teufels.com>,
    Georg Kathan <g.kathan@teufels.com>,
    Hendrik Krüger <h.krueger@teufels.com>,
    Perrin Ennen <p.ennen@teufels.com>,
    Timo Bittner <t.bittner@teufels.com>,
    teufels GmbH <digital@teufels.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
